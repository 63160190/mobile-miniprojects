import 'Cars.dart';
import 'ThunderKick.dart';

class Nissan extends Cars with ThunderKick {
  String? name = "Nissan";
  String? model = "GTR R35";
  String? color = "White";
  int hp = 3350;
  int attack = 330;
  int defense = 580;

  Nissan() {
    this.name = name;
    this.model = model;
    this.color = color;
    this.hp = hp;
    this.attack = attack;
    this.defense = defense;
  }
}
