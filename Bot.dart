import 'Cars.dart';
import 'SpiralArrow.dart';

class Bot extends Cars with SpiralArrow {
  int attack = 335;
  String? color = "Black";
  int defense = 490;
  int hp = 4000;
  String? model = "R8 RS";
  String? name = "Audi";

  @override
  atk(int atk) {
    hp = hp - atk;
    return hp;
  }

  @override
  def(int atk) {
    if (atk >= defense) {
      atk = atk - defense;
      hp = hp - atk;
      return hp;
    } else if (atk < defense) {
      return hp;
    }
  }

  @override
  void die() {
    print("Player Win!!!");
  }

  @override
  void showAll() {
    print(("name: " +
        name.toString() +
        "\n" +
        "model: " +
        model.toString() +
        "\n" +
        "Color: " +
        color.toString() +
        "\n" +
        "Hp: " +
        hp.toString() +
        "\n" +
        "Attack: " +
        attack.toString() +
        "\n" +
        "Defense " +
        defense.toString()));
  }

  spiralarrow() {
    int combo = 700;
    combo = attack + combo;
    return combo;
  }
}
