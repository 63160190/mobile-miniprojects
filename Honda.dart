import 'Cars.dart';
import 'ThunderKick.dart';

class Honda extends Cars with ThunderKick {
  String? name = "Honda";
  String? model = "Acura Nsx";
  String? color = "Gray";
  int hp = 3200;
  int attack = 320;
  int defense = 500;

  Honda() {
    this.name = name;
    this.model = model;
    this.color = color;
    this.hp = hp;
    this.attack = attack;
    this.defense = defense;
  }
}
