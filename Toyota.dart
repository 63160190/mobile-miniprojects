import 'Cars.dart';
import 'Hadouken.dart';

class Toyota extends Cars with Hadouken {
  String? name = "Toyota";
  String? model = "GR supra";
  String? color = "Red";
  int hp = 3100;
  int attack = 310;
  int defense = 440;
  //"Toyota", "GR supra", "Red", 11000, 250, 440);
  Toyota() {
    this.name = name;
    this.model = model;
    this.color = color;
    this.hp = hp;
    this.attack = attack;
    this.defense = defense;
  }
}
