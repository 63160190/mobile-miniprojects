import 'Cars.dart';
import 'Hadouken.dart';
import 'SpiralArrow.dart';
import 'ThunderKick.dart';

class Player extends Cars with ThunderKick, Hadouken {
  int attack = 0;
  String? color;
  int defense = 0;
  int hp = 0;
  String? model;
  String? name;
  int combo = 0;

  @override
  atk(int atk) {
    hp = hp - atk;
    return hp;
  }

  @override
  def(int atk) {
    if (atk >= defense) {
      atk = atk - defense;
      hp = hp - atk;
      return hp;
    } else if (atk < defense) {
      return hp;
    }
  }

  @override
  die() {
    print("YoU Lose");
  }

  @override
  void showAll() {
    print(("name: " +
        name.toString() +
        "\n" +
        "model: " +
        model.toString() +
        "\n" +
        "Color: " +
        color.toString() +
        "\n" +
        "Hp: " +
        hp.toString() +
        "\n" +
        "Attack: " +
        attack.toString() +
        "\n" +
        "Defense " +
        defense.toString()));
  }

  thunderkick() {
    combo = 500;
    combo = attack + combo;
    return combo;
  }

  Hadouken() {
    combo = 600;
    combo = attack + combo;
    return combo;
  }
}
